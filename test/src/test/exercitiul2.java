package test;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Flow;
import javax.swing.*;

public class exercitiul2 {

    public static void main(String[] args){
        JFrame frame=new JFrame();


        JButton button=new JButton("Copy");
        JTextField field1=new JTextField();
        JTextField field2=new JTextField();

        field1.setBounds(0,0,300,30);
        field2.setBounds(0,66,300,30);
        button.setBounds(0,33,300,30);

        field2.setEditable(false);

        frame.setLayout(null);
        frame.setSize(300,150);

        frame.add(field1);
        frame.add(button);
        frame.add(field2);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                field2.setText(field1.getText());
            }
        });

        frame.setVisible(true);
    }
}